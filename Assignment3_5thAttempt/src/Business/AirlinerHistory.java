/*
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 * @author Samsung
 */
public class AirlinerHistory    {
    private ArrayList<Airliner> airlinerHistory;

public AirlinerHistory()    {
    airlinerHistory = new ArrayList<Airliner>();
}    

    public ArrayList<Airliner> getAirlinerHistory() {
        return airlinerHistory;
    }

    public void setAirlinerHistory(ArrayList<Airliner> airlinerHistory) {
        this.airlinerHistory = airlinerHistory;
    }
    
    public Airliner addairliner()   {
        Airliner a = new Airliner();
        airlinerHistory.add(a);
        return a;
        
    }
    
    public void deleteairliner(Airliner a)  { 
           airlinerHistory.remove(a);
    }    
}
    


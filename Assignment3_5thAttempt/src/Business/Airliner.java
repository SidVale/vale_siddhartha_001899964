/*
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 * @author Samsung
 */
public class Airliner {
    private String flightNum;
    private String model;
    private String source;
    private String destination;
    private String depDate;
    private String airlinerName;
    private String preferredTime;

    public String getAirlinerName() {
        return airlinerName;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setAirlinerName(String airlinerName) {
        this.airlinerName = airlinerName;
    }

      public String getFlightNum() {
        return flightNum;
    }

    public void setFlightNum(String flightNum) {
        this.flightNum = flightNum;
    }

    public String getDepDate() {
        return depDate;
    }

    public void setDepDate(String depDate) {
        this.depDate = depDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getPreferredTime() {
        return preferredTime;
    }

    public void setPreferredTime(String preferredTime) {
        this.preferredTime = preferredTime;
    }
    
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    private Customer customer;
    
    @Override
    public String toString(){
        return airlinerName;
    }
}

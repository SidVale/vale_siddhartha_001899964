/*
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 * @author Samsung
 */
public class Customer {
    private String firstName;
    private String lastName;
    private String passportNum;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSsnNum() {
        return passportNum;
    }

    public void setSsnNum(String ssnNum) {
        this.passportNum = passportNum;
    }
}

/*
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.ManageCustomers;

import Business.Airliner;
import Business.AirlinerHistory;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 * @author Samsung
 */
public class ManageCustomerJPanel extends javax.swing.JPanel {
 private JPanel cardSequenceContainer;
    private AirlinerHistory airlinerHistory;
    private Airliner airliner;   

    public ManageCustomerJPanel(JPanel cardSequenceContainer, AirlinerHistory airlinerHistory) {
        initComponents();
       this.cardSequenceContainer = cardSequenceContainer;
       this.airlinerHistory = airlinerHistory;
    }
    
    private void populateTable(){
        DefaultTableModel dtm = (DefaultTableModel) customerBookingTable.getModel();
        dtm.setRowCount(0);
        for(Airliner airliner : airlinerHistory.getAirlinerHistory()){
            Object[] row = new Object[5];
            row[0] = airliner;
            row[1] = airliner.getFlightNum();
            row[2] = airliner.getSource();
            row[3] = airliner.getDestination();
            row[4] = airliner.getPreferredTime();
            dtm.addRow(row);
            
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        customerBookingTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        bookTicketButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        customerBookingTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Airline Name", "Flight Number", "Departure Date", "Origin", "Destination", "Time"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(customerBookingTable);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Customer Booking Page");

        bookTicketButton.setBackground(new java.awt.Color(51, 51, 51));
        bookTicketButton.setForeground(new java.awt.Color(255, 255, 255));
        bookTicketButton.setText("Book Ticket");
        bookTicketButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bookTicketButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(326, 326, 326)
                .addComponent(jLabel1)
                .addContainerGap(409, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(bookTicketButton, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(43, 43, 43)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 267, Short.MAX_VALUE)
                .addComponent(bookTicketButton, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void bookTicketButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bookTicketButtonActionPerformed
        // TODO add your handling code here:
    
    }//GEN-LAST:event_bookTicketButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bookTicketButton;
    private javax.swing.JTable customerBookingTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
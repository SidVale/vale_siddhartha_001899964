/*
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.AddAirline;

import Business.Airliner;
import Business.AirlinerHistory;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * @author Samsung
 */

public class AddAirlines extends javax.swing.JPanel {
    JPanel userProcessContainer;
    AirlinerHistory airlinerHistory;

    public AddAirlines(JPanel upc, AirlinerHistory ad) {
         initComponents();
        userProcessContainer = upc;
        airlinerHistory = ad;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        airlineNameLabel = new javax.swing.JLabel();
        airlineNameTextField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        addAirlineButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        airlineNameLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        airlineNameLabel.setText("Airline Name");

        airlineNameTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                airlineNameTextFieldActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("New Airline");

        addAirlineButton.setBackground(new java.awt.Color(51, 51, 51));
        addAirlineButton.setForeground(new java.awt.Color(255, 255, 255));
        addAirlineButton.setText("Add Airline");
        addAirlineButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAirlineButtonActionPerformed(evt);
            }
        });

        backButton.setBackground(new java.awt.Color(51, 51, 51));
        backButton.setForeground(new java.awt.Color(255, 255, 255));
        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(backButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addAirlineButton))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(airlineNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(airlineNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 322, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(164, 164, 164)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(airlineNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(airlineNameLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 288, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addAirlineButton)
                    .addComponent(backButton))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void addAirlineButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addAirlineButtonActionPerformed
        // TODO add your handling code here:

        String airline = airlineNameTextField.getText();
         
        Airliner a = airlinerHistory.addairliner();
        a.setAirlinerName(airline);
        
        JOptionPane.showMessageDialog(null,"Account is Sucessfully Created");
     
    }//GEN-LAST:event_addAirlineButtonActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        // TODO add your handling code here:
     
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
        
    }//GEN-LAST:event_backButtonActionPerformed

    private void airlineNameTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_airlineNameTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_airlineNameTextFieldActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addAirlineButton;
    private javax.swing.JLabel airlineNameLabel;
    private javax.swing.JTextField airlineNameTextField;
    private javax.swing.JButton backButton;
    private javax.swing.JLabel jLabel5;
    // End of variables declaration//GEN-END:variables
}
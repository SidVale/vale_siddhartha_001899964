/*
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.AddAirline;

import Business.AirlinerHistory;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 * @author Samsung
 */
public class AdminWorkAreaJPanel extends javax.swing.JPanel {
JPanel userProcessContainer;
    AirlinerHistory airlinerHistory;  

    public AdminWorkAreaJPanel(JPanel upc, AirlinerHistory ad) {
         initComponents();
        userProcessContainer = upc;
        airlinerHistory = ad;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        manageAirlinesButton = new javax.swing.JButton();
        addAirlineButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Travel Agency Work Area");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 630, 40));

        manageAirlinesButton.setBackground(new java.awt.Color(51, 51, 51));
        manageAirlinesButton.setForeground(new java.awt.Color(255, 255, 255));
        manageAirlinesButton.setText("Manage Airlines");
        manageAirlinesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                manageAirlinesButtonActionPerformed(evt);
            }
        });
        add(manageAirlinesButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 130, 270, 60));

        addAirlineButton.setBackground(new java.awt.Color(51, 51, 51));
        addAirlineButton.setForeground(new java.awt.Color(255, 255, 255));
        addAirlineButton.setText("Add Airline");
        addAirlineButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAirlineButtonActionPerformed(evt);
            }
        });
        add(addAirlineButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 203, 270, 70));
    }// </editor-fold>//GEN-END:initComponents

    private void manageAirlinesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manageAirlinesButtonActionPerformed
        // TODO add your handling code here:

        ManageAirlines panel = new ManageAirlines(userProcessContainer, airlinerHistory);
        userProcessContainer.add("ManageSupplierAdministrative", panel);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        layout.next(userProcessContainer);
        
    }//GEN-LAST:event_manageAirlinesButtonActionPerformed

    private void addAirlineButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addAirlineButtonActionPerformed
        // TODO add your handling code here:

        AddAirlines panel = new AddAirlines(userProcessContainer,airlinerHistory);
        userProcessContainer.add("AddAirlines", panel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_addAirlineButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addAirlineButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton manageAirlinesButton;
    // End of variables declaration//GEN-END:variables
}
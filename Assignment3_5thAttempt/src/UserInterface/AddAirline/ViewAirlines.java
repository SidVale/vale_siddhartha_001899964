/*
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.AddAirline;

import Business.Airliner;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 * @author Samsung
 */
public class ViewAirlines extends javax.swing.JPanel {
JPanel userProcessContainer;
    Airliner airliner;
   
    ViewAirlines(JPanel upc, Airliner a) {
        initComponents();
        userProcessContainer = upc;
        airliner = a;
        airlineName.setText(airliner.getAirlinerName());
          this.userProcessContainer = upc;
        this.airliner = a;
        populateAirlineDetails();    
    }
    
private void populateAirlineDetails() {
    flightNameTextField.setText(airliner.getAirlinerName());
    modelButton.setText(airliner.getFlightNum());
    arrivalLocationTextField.setText(airliner.getSource());
    departureLocationTextField.setText(airliner.getDestination());
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        backButton = new javax.swing.JButton();
        airlineName = new javax.swing.JLabel();
        departureLocationTextField = new javax.swing.JTextField();
        timeTextField = new javax.swing.JTextField();
        modelLabel = new javax.swing.JLabel();
        arrivalLocationLabel = new javax.swing.JLabel();
        departureLocationLabel = new javax.swing.JLabel();
        timeLabel = new javax.swing.JLabel();
        modelButton = new javax.swing.JTextField();
        arrivalLocationTextField = new javax.swing.JTextField();
        flightNameLabel = new javax.swing.JLabel();
        flightNameTextField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        backButton.setBackground(new java.awt.Color(51, 51, 51));
        backButton.setForeground(new java.awt.Color(255, 255, 255));
        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        airlineName.setBackground(new java.awt.Color(255, 255, 255));

        departureLocationTextField.setEditable(false);
        departureLocationTextField.setBackground(new java.awt.Color(255, 255, 255));
        departureLocationTextField.setEnabled(false);

        timeTextField.setEditable(false);
        timeTextField.setBackground(new java.awt.Color(255, 255, 255));
        timeTextField.setEnabled(false);

        modelLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        modelLabel.setText("Model");

        arrivalLocationLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        arrivalLocationLabel.setText("Arrival Location");

        departureLocationLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        departureLocationLabel.setText("Departure Location");

        timeLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        timeLabel.setText("Time");

        modelButton.setEditable(false);
        modelButton.setBackground(new java.awt.Color(255, 255, 255));
        modelButton.setEnabled(false);
        modelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modelButtonActionPerformed(evt);
            }
        });

        arrivalLocationTextField.setEditable(false);
        arrivalLocationTextField.setBackground(new java.awt.Color(255, 255, 255));
        arrivalLocationTextField.setEnabled(false);
        arrivalLocationTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                arrivalLocationTextFieldActionPerformed(evt);
            }
        });

        flightNameLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        flightNameLabel.setText("Flight Name");

        flightNameTextField.setEditable(false);
        flightNameTextField.setBackground(new java.awt.Color(255, 255, 255));
        flightNameTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                flightNameTextFieldActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("View Airlines");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(350, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                .addGap(368, 368, 368))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(backButton, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(108, 108, 108)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(flightNameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(modelLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(arrivalLocationLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 92, Short.MAX_VALUE)
                                .addComponent(timeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(departureLocationLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(airlineName, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(modelButton, javax.swing.GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
                                .addComponent(arrivalLocationTextField)
                                .addComponent(departureLocationTextField)
                                .addComponent(timeTextField)
                                .addComponent(flightNameTextField)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addComponent(airlineName, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(flightNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(flightNameLabel))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(modelLabel)
                    .addComponent(modelButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(arrivalLocationLabel)
                    .addComponent(arrivalLocationTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(departureLocationLabel)
                    .addComponent(departureLocationTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(timeLabel)
                    .addComponent(timeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(218, 218, 218)
                .addComponent(backButton)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        // TODO add your handling code here:
    
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
        
    }//GEN-LAST:event_backButtonActionPerformed

    private void flightNameTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_flightNameTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_flightNameTextFieldActionPerformed

    private void modelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modelButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_modelButtonActionPerformed

    private void arrivalLocationTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_arrivalLocationTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_arrivalLocationTextFieldActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel airlineName;
    private javax.swing.JLabel arrivalLocationLabel;
    private javax.swing.JTextField arrivalLocationTextField;
    private javax.swing.JButton backButton;
    private javax.swing.JLabel departureLocationLabel;
    private javax.swing.JTextField departureLocationTextField;
    private javax.swing.JLabel flightNameLabel;
    private javax.swing.JTextField flightNameTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JTextField modelButton;
    private javax.swing.JLabel modelLabel;
    private javax.swing.JLabel timeLabel;
    private javax.swing.JTextField timeTextField;
    // End of variables declaration//GEN-END:variables
}
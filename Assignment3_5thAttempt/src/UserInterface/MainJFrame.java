/*
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.Airliner;
import Business.AirlinerHistory;
import UserInterface.ManageAirliners.ManageAirlinerJPanel;
import UserInterface.ManageCustomers.ManageCustomerJPanel;
import UserInterface.AddAirline.AdminWorkAreaJPanel;
import java.awt.CardLayout;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


/**
 * @author Samsung
 */
public class MainJFrame extends javax.swing.JFrame {
private AirlinerHistory airlinerHistory;
   
    public MainJFrame() {
        initComponents();
        airlinerHistory = new AirlinerHistory();
        display();
    }
 
private void display() {
        String excelFile = "C:\\Users\\Lenovo-1\\Documents\\fleet.csv";
        BufferedReader bufferReader = null;
        String line = "";
        String separator = ",";
        
    try {     
        bufferReader = new BufferedReader(new FileReader(excelFile));
        while ((line = bufferReader.readLine()) != null)    {
          String[] details = line.split(separator);
          
            Airliner airliner = airlinerHistory.addairliner();
          
            airliner.setAirlinerName(details[0]);
            airliner.setFlightNum(details[1]);
            airliner.setPreferredTime(details[2]);
            airliner.setSource(details[3]);
            airliner.setDestination(details[4]); 
        }   
        
    }
    
    catch (FileNotFoundException e) {
        e.printStackTrace();
    } 
    catch (IOException e)   {
        e.printStackTrace(); 
    }
}

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        AbosluteJPanel = new javax.swing.JPanel();
        addAirlineButton = new javax.swing.JButton();
        manageAirlinerButton = new javax.swing.JButton();
        manageCustomerButton = new javax.swing.JButton();
        cardSequenceContainer = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jSplitPane1.setBackground(new java.awt.Color(255, 255, 255));
        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        AbosluteJPanel.setBackground(new java.awt.Color(255, 255, 255));
        AbosluteJPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        addAirlineButton.setBackground(new java.awt.Color(204, 204, 204));
        addAirlineButton.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        addAirlineButton.setText("Add Airliner");
        addAirlineButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAirlineButtonActionPerformed(evt);
            }
        });
        AbosluteJPanel.add(addAirlineButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 110, 80));

        manageAirlinerButton.setBackground(new java.awt.Color(204, 204, 204));
        manageAirlinerButton.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        manageAirlinerButton.setText("Manage Fleet");
        manageAirlinerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                manageAirlinerButtonActionPerformed(evt);
            }
        });
        AbosluteJPanel.add(manageAirlinerButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 10, 110, 80));

        manageCustomerButton.setBackground(new java.awt.Color(102, 102, 102));
        manageCustomerButton.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        manageCustomerButton.setForeground(new java.awt.Color(255, 255, 255));
        manageCustomerButton.setText("Manage Customer");
        manageCustomerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                manageCustomerButtonActionPerformed(evt);
            }
        });
        AbosluteJPanel.add(manageCustomerButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 10, 150, 80));

        jSplitPane1.setLeftComponent(AbosluteJPanel);

        cardSequenceContainer.setBackground(new java.awt.Color(255, 255, 255));
        cardSequenceContainer.setLayout(new java.awt.CardLayout());
        jSplitPane1.setRightComponent(cardSequenceContainer);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 489, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addAirlineButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addAirlineButtonActionPerformed
       // TODO add your handling code here:
       
        AdminWorkAreaJPanel panel = new AdminWorkAreaJPanel(cardSequenceContainer, airlinerHistory);
        cardSequenceContainer.add("TravelAgencyJPanel", panel);
        CardLayout layout = (CardLayout) cardSequenceContainer.getLayout();
        layout.next(cardSequenceContainer);
        
    }//GEN-LAST:event_addAirlineButtonActionPerformed

    private void manageAirlinerButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manageAirlinerButtonActionPerformed
        // TODO add your handling code here:

        ManageAirlinerJPanel panel = new ManageAirlinerJPanel(cardSequenceContainer, airlinerHistory);
        cardSequenceContainer.add("ManageAirlinersJPanel", panel);
        CardLayout layout = (CardLayout) cardSequenceContainer.getLayout();
        layout.next(cardSequenceContainer);

    }//GEN-LAST:event_manageAirlinerButtonActionPerformed

    private void manageCustomerButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manageCustomerButtonActionPerformed
        // TODO add your handling code here:

        ManageCustomerJPanel panel =  new ManageCustomerJPanel(cardSequenceContainer, airlinerHistory);
        cardSequenceContainer.add("CreateCustomerJPanel", panel);
        CardLayout layout = (CardLayout) cardSequenceContainer.getLayout();
        layout.next(cardSequenceContainer);
        
    }//GEN-LAST:event_manageCustomerButtonActionPerformed
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainJFrame().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel AbosluteJPanel;
    private javax.swing.JButton addAirlineButton;
    private javax.swing.JPanel cardSequenceContainer;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JButton manageAirlinerButton;
    private javax.swing.JButton manageCustomerButton;
    // End of variables declaration//GEN-END:variables
}
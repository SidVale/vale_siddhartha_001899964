/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;
import Business.CarsHistory;
import Business.Cars;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author Samsung
 */
public class DisplayJPanel extends javax.swing.JPanel
{
    
     public void SimpleDateFormat() {  
        Date date = new Date();  
        SimpleDateFormat formatter = new SimpleDateFormat("Last Updated:" + "dd/MM/yyyy hh:mm:ss");  
        String lastUpdatedLabel= formatter.format(date);  
        System.out.println(lastUpdatedLabel);  
}  
    
    /**
     * Creates new form DisplayJPanel
     */
    private CarsHistory ch;
    ButtonGroup BTNgrp;
    String[] choose = new String[11];
    public DisplayJPanel(CarsHistory ch)
    {
        initComponents();
        this.ch = ch;
        populateTable();
        
        
        
        choose[0] = "Find me the first available passenger car";
        choose[1] = "How many cars are currently available";
        choose[2] = "List all cars that are made by Honda";
        choose[3] = "List all cars that are of Model Year 2012";
        choose[4] = "Find an available car with a given minimum and maximum number of seats";
        choose[5] = "Find a car with a Serial Number of 1234 . List the attributes of the found car";
        choose[6] = "List all cars with model name Camry";
        choose[7] = "List all the car make's used by the John";
        choose[8] = "List all cars that are available in Boston";
        choose[9] = "List all cars that have expired maintenance certificate";
        
            BTNgrp = new ButtonGroup();
            BTNgrp.add(yesRadioButton);
            BTNgrp.add(noRadioButton);
            
            populateFilter();
            
    }
    public void populateFilter()
    {
        DefaultTableModel dtm = (DefaultTableModel)filterTabel.getModel();
        dtm.setRowCount(0);
        
        for(String c : choose)
        {
            Object row[] = new Object[2];
            row[0] = c;    
            dtm.addRow(row);
            
        }
    }
    
    public void populateTable()
    {
        DefaultTableModel dtm = (DefaultTableModel)carAvailablityTable.getModel();
        dtm.setRowCount(0);
        
        for(Cars c : ch.getCarsHistory())
        {
            Object row[] = new Object[2];
            row[0] = c; 
            row[1] = c.getAvailability();   
            dtm.addRow(row);
            
        }
    }
    
    public void displayFirstAvailability()
    {
        DefaultTableModel dtm = (DefaultTableModel)carAvailablityTable.getModel();
        dtm.setRowCount(0);
        
        for(Cars c : ch.getCarsHistory())
        {
            if(c.getAvailability() == true)
            {
            Object row[] = new Object[2];
            row[0] = c; 
            row[1] = c.getAvailability();   
            dtm.addRow(row);
            break;
            }
            
        }
    }
    
    public void displayAllAvailability()
    {
        DefaultTableModel dtm = (DefaultTableModel)carAvailablityTable.getModel();
        dtm.setRowCount(0);
        
        for(Cars c : ch.getCarsHistory())
        {
            if(c.getAvailability() == true)
            {
            Object row[] = new Object[2];
            row[0] = c; 
            row[1] = c.getAvailability();   
            dtm.addRow(row);
            }
            
        }
    }
    
    public void displayMake()
    {
        DefaultTableModel dtm = (DefaultTableModel)carAvailablityTable.getModel();
        dtm.setRowCount(0);
        
        for(Cars c : ch.getCarsHistory())
        {
            if(c.getMake().equals("Honda"))
            {
            Object row[] = new Object[2];
            row[0] = c; 
            row[1] = c.getAvailability();   
            dtm.addRow(row);
            }
            
        }
    }
    
    public void displayModelYear()
    {
        DefaultTableModel dtm = (DefaultTableModel)carAvailablityTable.getModel();
        dtm.setRowCount(0);
        
        
        for(Cars c : ch.getCarsHistory())
        {    
            if(c.getModelYear()==2012)
            {
            Object row[] = new Object[2];
            row[0] = c; 
            row[1] = c.getAvailability();   
            dtm.addRow(row);
            }
            
        }
    }
    
     public void displaySeat()
    {
        DefaultTableModel dtm = (DefaultTableModel)carAvailablityTable.getModel();
        dtm.setRowCount(0);
        
        for(Cars c : ch.getCarsHistory())
        {
            if(c.getMinimumSeat() <=  Integer.parseInt(minSeatTextField.getText()) && c.getMaximumSeat() < Integer.parseInt(maxSeatTextField.getText()))
            {
            Object row[] = new Object[2];
            row[0] = c; 
            row[1] = c.getAvailability();   
            dtm.addRow(row);
            }
            
        }
    }
    
     public void displaySerialNumber()
    {
        DefaultTableModel dtm = (DefaultTableModel)carAvailablityTable.getModel();
        dtm.setRowCount(0);
        
        
        for(Cars c : ch.getCarsHistory())
        {
            if(c.getSerialNumber()==1234)
            {
            Object row[] = new Object[2];
            row[0] = c; 
            row[1] = c.getAvailability();   
            dtm.addRow(row);
            }
            
        }
    }
        public void displayModel()
    {
        DefaultTableModel dtm = (DefaultTableModel)carAvailablityTable.getModel();
        dtm.setRowCount(0);
        
        for(Cars c : ch.getCarsHistory())
        {
            if(c.getModel().equals("Camry"))
            {
            Object row[] = new Object[2];
            row[0] = c; 
            row[1] = c.getAvailability();   
            dtm.addRow(row);
            }
            
        }
    }
        
        public void displayFirstName()
    {
        DefaultTableModel dtm = (DefaultTableModel)carAvailablityTable.getModel();
        dtm.setRowCount(0);
        
        for(Cars c : ch.getCarsHistory())
        {
            if(c.getFirstName().equals("John"))
            {
            Object row[] = new Object[2];
            row[0] = c; 
            row[1] = c.getAvailability();   
            dtm.addRow(row);
            }
            
        }
    }
        
        public void displayCity()
    {
        DefaultTableModel dtm = (DefaultTableModel)carAvailablityTable.getModel();
        dtm.setRowCount(0);
        
        for(Cars c : ch.getCarsHistory())
        {
            if(c.getCity().equals("Boston"))
            {
            Object row[] = new Object[2];
            row[0] = c; 
            row[1] = c.getAvailability();   
            dtm.addRow(row);
            }
            
        }
    }
       
        public void displayMaintainenceCertificate()
    {
        DefaultTableModel dtm = (DefaultTableModel)carAvailablityTable.getModel();
        dtm.setRowCount(0);
        
        for(Cars c : ch.getCarsHistory())
        {
            if(c.getMaintainenceExpiry()<2018)
            {
            Object row[] = new Object[2];
            row[0] = c; 
            row[1] = c.getAvailability();   
            dtm.addRow(row);
            }
            
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        displayUpdateVehicleLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        carAvailablityTable = new javax.swing.JTable();
        updateButton = new javax.swing.JButton();
        displayButton = new javax.swing.JButton();
        vehicleDetailsLabel = new javax.swing.JLabel();
        makeLabel = new javax.swing.JLabel();
        makeTextField = new javax.swing.JTextField();
        yesRadioButton = new javax.swing.JRadioButton();
        noRadioButton = new javax.swing.JRadioButton();
        availabilityLabel = new javax.swing.JLabel();
        lastNameTextField = new javax.swing.JTextField();
        lastNameLabel = new javax.swing.JLabel();
        driverLicenceNoTextField = new javax.swing.JTextField();
        firstNameTextField = new javax.swing.JTextField();
        firstNameLabel = new javax.swing.JLabel();
        driverLicenceNoLabel = new javax.swing.JLabel();
        driverDetailsLabel = new javax.swing.JLabel();
        modelYearLabel = new javax.swing.JLabel();
        modelLabel = new javax.swing.JLabel();
        modelTextField = new javax.swing.JTextField();
        modelYearTextField = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        filterTabel = new javax.swing.JTable();
        lastUpdatedLabel = new javax.swing.JLabel();
        minimumSeatTextField = new javax.swing.JTextField();
        maintainenceExpiryTextField = new javax.swing.JTextField();
        maximumSeatTextField = new javax.swing.JTextField();
        maximumSeatLabel = new javax.swing.JLabel();
        minimumSeatLabel = new javax.swing.JLabel();
        maintainenceExpiryLabel = new javax.swing.JLabel();
        saveButton = new javax.swing.JButton();
        serialNumberTextField = new javax.swing.JTextField();
        serialNumberLabel = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        cityTextField = new javax.swing.JTextField();
        cityLabel = new javax.swing.JLabel();
        minSeatLabel = new javax.swing.JLabel();
        maxSeatLabel = new javax.swing.JLabel();
        minSeatTextField = new javax.swing.JTextField();
        maxSeatTextField = new javax.swing.JTextField();

        displayUpdateVehicleLabel.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        displayUpdateVehicleLabel.setText("Display/Update Vehicle");

        carAvailablityTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Vehicle Make", "Availability"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(carAvailablityTable);
        if (carAvailablityTable.getColumnModel().getColumnCount() > 0) {
            carAvailablityTable.getColumnModel().getColumn(0).setResizable(false);
            carAvailablityTable.getColumnModel().getColumn(1).setResizable(false);
        }

        updateButton.setText("Edit");
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });

        displayButton.setText("Display");
        displayButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                displayButtonActionPerformed(evt);
            }
        });

        vehicleDetailsLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        vehicleDetailsLabel.setText("Vehicle Details");

        makeLabel.setText("Make:");

        makeTextField.setEnabled(false);

        yesRadioButton.setText("Yes");
        yesRadioButton.setEnabled(false);

        noRadioButton.setText("No");
        noRadioButton.setEnabled(false);

        availabilityLabel.setText("Availability:");

        lastNameTextField.setEnabled(false);

        lastNameLabel.setText("Last Name:");

        driverLicenceNoTextField.setEnabled(false);

        firstNameTextField.setEnabled(false);

        firstNameLabel.setText("First Name:");

        driverLicenceNoLabel.setText("Driver Licence No.:");

        driverDetailsLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        driverDetailsLabel.setText("Driver Details");

        modelYearLabel.setText("Model Year:");

        modelLabel.setText("Model:");

        modelTextField.setEnabled(false);

        modelYearTextField.setEnabled(false);

        filterTabel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Select Filter"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        filterTabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                filterTabelMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(filterTabel);
        if (filterTabel.getColumnModel().getColumnCount() > 0) {
            filterTabel.getColumnModel().getColumn(0).setResizable(false);
        }

        lastUpdatedLabel.setText("Last Updated:");

        minimumSeatTextField.setEnabled(false);

        maintainenceExpiryTextField.setEnabled(false);

        maximumSeatTextField.setEnabled(false);

        maximumSeatLabel.setText("Maximum Seat:");

        minimumSeatLabel.setText("MinimumSeat :");

        maintainenceExpiryLabel.setText("Maintainence Expiry:");

        saveButton.setText("Save");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });

        serialNumberTextField.setEnabled(false);

        serialNumberLabel.setText("Serial Number:");

        jButton1.setText("Select");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        cityTextField.setEnabled(false);

        cityLabel.setText("City:");

        minSeatLabel.setText("Min seat:");

        maxSeatLabel.setText("Max seat:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(403, 403, 403)
                        .addComponent(saveButton))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(45, 45, 45)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addGap(33, 33, 33))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(minSeatLabel)
                                    .addComponent(maxSeatLabel))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(maxSeatTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
                                    .addComponent(minSeatTextField)))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(156, 156, 156)
                                .addComponent(vehicleDetailsLabel))
                            .addComponent(displayUpdateVehicleLabel)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(updateButton, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)
                                    .addComponent(displayButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(modelYearLabel, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(181, 181, 181)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(modelLabel)
                                                    .addComponent(makeLabel))))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(modelTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
                                            .addComponent(makeTextField, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(modelYearTextField, javax.swing.GroupLayout.Alignment.TRAILING)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(156, 156, 156)
                                        .addComponent(availabilityLabel)
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(noRadioButton)
                                            .addComponent(yesRadioButton))))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(maximumSeatLabel)
                                            .addComponent(minimumSeatLabel)
                                            .addComponent(maintainenceExpiryLabel)
                                            .addComponent(serialNumberLabel))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(serialNumberTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(minimumSeatTextField)
                                                .addComponent(maintainenceExpiryTextField)
                                                .addComponent(maximumSeatTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(cityLabel)
                                        .addGap(18, 18, 18)
                                        .addComponent(cityTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(lastUpdatedLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(43, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 116, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(driverLicenceNoLabel, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(firstNameLabel, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(driverLicenceNoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(firstNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(39, 39, 39)
                                .addComponent(lastNameLabel)
                                .addGap(18, 18, 18)
                                .addComponent(lastNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addComponent(driverDetailsLabel)))
                .addGap(84, 84, 84))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(displayUpdateVehicleLabel)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(36, 36, 36)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(displayButton)
                                .addGap(18, 18, 18)
                                .addComponent(updateButton)))
                        .addGap(28, 28, 28)
                        .addComponent(vehicleDetailsLabel)
                        .addGap(14, 14, 14)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(makeLabel)
                                    .addComponent(makeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(modelLabel)
                                    .addComponent(modelTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(21, 21, 21)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(modelYearLabel)
                                    .addComponent(modelYearTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(maintainenceExpiryLabel)
                                    .addComponent(maintainenceExpiryTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(minimumSeatLabel)
                                    .addComponent(minimumSeatTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(maximumSeatTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(maximumSeatLabel))))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(availabilityLabel)
                            .addComponent(yesRadioButton)
                            .addComponent(serialNumberLabel)
                            .addComponent(serialNumberTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(minSeatTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(minSeatLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(maxSeatLabel)
                            .addComponent(maxSeatTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29)
                        .addComponent(jButton1)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(noRadioButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cityLabel)
                            .addComponent(cityTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(21, 21, 21)))
                .addComponent(driverDetailsLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(firstNameLabel)
                        .addComponent(firstNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lastNameLabel)
                        .addComponent(lastNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(driverLicenceNoLabel)
                    .addComponent(driverLicenceNoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lastUpdatedLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(saveButton, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void displayButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_displayButtonActionPerformed
        // TODO add your handling code here:
        int selectedrow = carAvailablityTable.getSelectedRow();
   
        if(selectedrow >=0)
        {
            Cars c = (Cars)carAvailablityTable.getValueAt(selectedrow, 0);
            makeTextField.setText(String.valueOf(c.getMake()));
            modelTextField.setText(String.valueOf(c.getModel()));
            firstNameTextField.setText(String.valueOf(c.getFirstName()));
            lastNameTextField.setText(String.valueOf(c.getLastName()));
            minimumSeatTextField.setText(String.valueOf(c.getMinimumSeat()));
            maximumSeatTextField.setText(String.valueOf(c.getMaximumSeat()));
            modelYearTextField.setText(String.valueOf(c.getModelYear()));
            maintainenceExpiryTextField.setText(String.valueOf(c.getMaintainenceExpiry()));
            driverLicenceNoTextField.setText(String.valueOf(c.getDriverLicenceNo()));
            serialNumberTextField.setText(String.valueOf(c.getSerialNumber()));
            cityTextField.setText(String.valueOf(c.getCity()));
            
            if(c.getAvailability() == true)
            {
                yesRadioButton.setSelected(true);
                noRadioButton.setSelected(false);
            }
            else
            {
                noRadioButton.setSelected(true);
                yesRadioButton.setSelected(false);
            }
            
        }   
        else
            JOptionPane.showMessageDialog(null, "Select the row you wish to display");
        
    }//GEN-LAST:event_displayButtonActionPerformed

    private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed
        // TODO add your handling code here:\
        
         int selectedrow = carAvailablityTable.getSelectedRow();
   
        if(selectedrow >=0)
        {
        Cars c = (Cars)carAvailablityTable.getValueAt(selectedrow, 0);
        makeTextField.setEnabled(true);
        modelTextField.setEnabled(true);
        modelYearTextField.setEnabled(true);
        minimumSeatTextField.setEnabled(true);
        maximumSeatTextField.setEnabled(true);
        maintainenceExpiryTextField.setEnabled(true);
        firstNameTextField.setEnabled(true);
        lastNameTextField.setEnabled(true);
        driverLicenceNoTextField.setEnabled(true);
        serialNumberTextField.setEnabled(true);
        yesRadioButton.setEnabled(true);
        noRadioButton.setEnabled(true);
        cityTextField.setEnabled(true);
        
        
        }
        else
            JOptionPane.showMessageDialog(null, "Select the row you wish to display");
        
        
    }//GEN-LAST:event_updateButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        // TODO add your handling code here:
        
        int selectedrow = carAvailablityTable.getSelectedRow();
   
        if(selectedrow >=0)
        {
        Cars c = (Cars)carAvailablityTable.getValueAt(selectedrow, 0);
        c.setMake(makeTextField.getText());
        c.setModel(modelTextField.getText());
        c.setFirstName(firstNameTextField.getText());
        c.setLastName(lastNameTextField.getText());
        c.setMaximumSeat(Integer.parseInt(maximumSeatTextField.getText()));
        c.setMinimumSeat(Integer.parseInt(minimumSeatTextField.getText()));
        c.setModelYear(Integer.parseInt(modelYearTextField.getText()));
        c.setMaintainenceExpiry(Integer.parseInt(maintainenceExpiryTextField.getText()));
        c.setDriverLicenceNo(Integer.parseInt(driverLicenceNoTextField.getText()));
        c.setSerialNumber(Integer.parseInt(serialNumberTextField.getText()));
        c.setCity(cityTextField.getText());
        Boolean availability;         
        JOptionPane.showMessageDialog(null, "Vehicle & Driver Information updated");
        if(BTNgrp.getSelection() == yesRadioButton.getModel())
                     
        {
            availability = true;
        }
        else
        {
            availability = false;
        }
        
        c.setAvailability(availability);
        
        }
        try
        {
            int minimumSeat = Integer.parseInt(minimumSeatTextField.getText());
        }
        catch (NumberFormatException e)
        {
            JOptionPane.showMessageDialog(null, "Invalid Entry, Minimum Seat must be a number");
        }
        
        try
        {
            int maximumSeat = Integer.parseInt(maximumSeatTextField.getText());
        }
        catch (NumberFormatException e)
        {
            JOptionPane.showMessageDialog(null, "Invalid Entry, Maximum Seat must be a number");
        }

        try
        {
            int modelYear = Integer.parseInt(modelYearTextField.getText());
        }
        catch
                (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Invalid Entry, Model Year must be in format YYYY");
        }        
        
        try
        {
            int driverLicenceNo = Integer.parseInt(driverLicenceNoTextField.getText());
        }
        catch (NumberFormatException e)
        {
            JOptionPane.showMessageDialog(null, "Invalid Entry, Driver Licence No. must be a number");
        }
        
    }//GEN-LAST:event_saveButtonActionPerformed

    private void filterTabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_filterTabelMouseClicked
        // TODO add your handling code here:
        
        
        
        
        
        
        
        
        
    }//GEN-LAST:event_filterTabelMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
      int selectedrow = filterTabel.getSelectedRow();
      
      switch(selectedrow)
      {
          case 0:{ displayFirstAvailability();
                  break;  }
          case 1:{ displayAllAvailability();
                  break;  }
          case 2:{ displayMake();
                  break; }
          case 3:{ displayModelYear();
                  break; }
          case 4:{ displaySeat();
                  break; }
          case 5:{ displaySerialNumber();
                  break; }
          case 6:{ displayModel();
                  break; }
          case 7:{ displayFirstName();
                  break; }
          case 8:{ displayCity();
                  break; }
          case 9:{ displayMaintainenceCertificate();
                  break; }
      }
      
        
        
        
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel availabilityLabel;
    private javax.swing.JTable carAvailablityTable;
    private javax.swing.JLabel cityLabel;
    private javax.swing.JTextField cityTextField;
    private javax.swing.JButton displayButton;
    private javax.swing.JLabel displayUpdateVehicleLabel;
    private javax.swing.JLabel driverDetailsLabel;
    private javax.swing.JLabel driverLicenceNoLabel;
    private javax.swing.JTextField driverLicenceNoTextField;
    private javax.swing.JTable filterTabel;
    private javax.swing.JLabel firstNameLabel;
    private javax.swing.JTextField firstNameTextField;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lastNameLabel;
    private javax.swing.JTextField lastNameTextField;
    private javax.swing.JLabel lastUpdatedLabel;
    private javax.swing.JLabel maintainenceExpiryLabel;
    private javax.swing.JTextField maintainenceExpiryTextField;
    private javax.swing.JLabel makeLabel;
    private javax.swing.JTextField makeTextField;
    private javax.swing.JLabel maxSeatLabel;
    private javax.swing.JTextField maxSeatTextField;
    private javax.swing.JLabel maximumSeatLabel;
    private javax.swing.JTextField maximumSeatTextField;
    private javax.swing.JLabel minSeatLabel;
    private javax.swing.JTextField minSeatTextField;
    private javax.swing.JLabel minimumSeatLabel;
    private javax.swing.JTextField minimumSeatTextField;
    private javax.swing.JLabel modelLabel;
    private javax.swing.JTextField modelTextField;
    private javax.swing.JLabel modelYearLabel;
    private javax.swing.JTextField modelYearTextField;
    private javax.swing.JRadioButton noRadioButton;
    private javax.swing.JButton saveButton;
    private javax.swing.JLabel serialNumberLabel;
    private javax.swing.JTextField serialNumberTextField;
    private javax.swing.JButton updateButton;
    private javax.swing.JLabel vehicleDetailsLabel;
    private javax.swing.JRadioButton yesRadioButton;
    // End of variables declaration//GEN-END:variables


    private static class Oject
    {

        public Oject()
        {
            
        }
    }
}

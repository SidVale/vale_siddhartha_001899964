/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 * @author Samsung
 */
public class Cars
{
    private String make;
    private String model;
    private String city;
    private int minimumSeat;
    private int maximumSeat;
    private int modelYear;
    private int maintainenceExpiry;
    private int serialNumber;
    private String firstName;
    private String lastName;
    private int driverLicenceNo;
    private Boolean availability;
    private int minSeat;
    private int maxSeat;

    public int getMinSeat() {
        return minSeat;
    }

    public void setMinSeat(int minSeat) {
        this.minSeat = minSeat;
    }

    public int getMaxSeat() {
        return maxSeat;
    }

    public void setMaxSeat(int maxSeat) {
        this.maxSeat = maxSeat;
    }
    
    

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }
    
    public int getMaintainenceExpiry() {
        return maintainenceExpiry;
    }

    public void setMaintainenceExpiry(int maintainenceExpiry) {
        this.maintainenceExpiry = maintainenceExpiry;
    }

    public Boolean getAvailability() {
        return availability;
    }

    public void setAvailability(Boolean availability) {
        this.availability = availability;
    }
    
    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getModelYear() {
        return modelYear;
    }

    public void setModelYear(int modelYear) {
        this.modelYear = modelYear;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getDriverLicenceNo() {
        return driverLicenceNo;
    }

    public void setDriverLicenceNo(int driverLicenceNo) {
        this.driverLicenceNo = driverLicenceNo;
    }

    public int getMinimumSeat() {
        return minimumSeat;
    }

    public void setMinimumSeat(int minimumSeat) {
        this.minimumSeat = minimumSeat;
    }

    public int getMaximumSeat() {
        return maximumSeat;
    }

    public void setMaximumSeat(int maximumSeat) {
        this.maximumSeat = maximumSeat;
    }
    
    @Override
    public String toString() {
        return make;
    }
     
}

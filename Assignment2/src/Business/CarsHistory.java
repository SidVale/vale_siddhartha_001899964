/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Samsung
 */
public class CarsHistory
{
   private ArrayList<Cars> carsHistory;
    
    public CarsHistory()
    {
        carsHistory = new ArrayList<Cars>();
    }

    public ArrayList<Cars> getCarsHistory()
    {
        return carsHistory;
    }

    public void setCarsHistory(ArrayList<Cars> carsHistory)
    {
        this.carsHistory = carsHistory;
    }
            
    public Cars addCars()
    {
        Cars cs = new Cars();
        carsHistory.add(cs);
        return cs;
    }
    
    public void deleteCars(Cars c)
    {
        carsHistory.remove(c);
    
    }
} 
